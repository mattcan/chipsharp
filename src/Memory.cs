using System;

namespace ChipSharp
{
    public class Memory
    {
        private const uint ROM_START_POSITION = 512;

        private readonly byte[] FONT_SPRITES = new byte[]{
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x80, 0x80, 0x80, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        };

        private byte[] _ram;

        public Memory() {
            this.Reset();
            this.Load(FONT_SPRITES, 0);
        }

        public byte GetOpcode (uint programCounter) {
            return Convert.ToByte(
                this.ReadAt(programCounter) << 8 | this.ReadAt(programCounter + 1)
            );
        }

        public bool LoadROM(byte[] rom) {
            if ((ROM_START_POSITION + rom.Length) > _ram.Length) {
                throw new IndexOutOfRangeException("ROM too large for memory");
            }

            try {
                this.Load(rom, ROM_START_POSITION);
                return true;
            } catch(Exception e) {
                return false;
            }
        }

        private byte ReadAt(uint location) {
            if (location > _ram.Length) {
                throw new IndexOutOfRangeException("Location is beyond length of RAM");
            }

            return _ram[location];
        }

        private void Load(byte[] data, uint position) {
            int numOfElements = data.Length;
            int romStart = 0;
            Array.Copy(data, romStart, _ram, position, numOfElements);
        }

        private void Reset() {
            _ram = new byte[4096];
        }
    }
}