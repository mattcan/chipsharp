using System;
using Xunit;
using ChipSharp;

namespace ChipSharpTest
{
    public class MemoryTests : IDisposable
    {
        Memory memory;

        public MemoryTests() {
            memory = new Memory();
        }

        public void Dispose() {
            memory = null;
        }

        [Fact]
        public void SuccessfulLoadROMTest() {
            byte[] rom = new byte[]{ 0xAA, 0xBB };
            bool result = memory.LoadROM(rom);

            Assert.True(result);
        }

        [Fact]
        public void ROMTooLargeErrorTest() {
            byte[] rom = new byte[4096];
            Assert.Throws<IndexOutOfRangeException>(() => { memory.LoadROM(rom); });
        }

        [Fact]
        public void GetOpcodeFromRamTest() {
            byte[] rom = new byte[]{ 0xAA, 0xBB };
            memory.LoadROM(rom);

            byte result = memory.GetOpcode(0x200);
            Assert.Equal(result, 0xAA);

        }
    }
}
